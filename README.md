## Yeeshkul-helper

This is a simple tool created for automatically downloading all torrent files that are referenced on a given page. This can be useful if you want to download all torrent files from one of the "Lists" threads, and add them to your torrent application.

** Note: this tool will not download anything but the .torrent-files. You need to use a normal torrent application to actually download the music "contained" in the torrent files. **

---

## Installation

You can either download the pre-compiled application from the [Executable folder](https://bitbucket.org/erlenddahl/yeeshkul-helper/raw/7bd87d8ec38dd1f0a3ca4e2f515a7631f70d2abc/Executable/Yeeshkul-helper%20WPF.exe), or you can clone the code repository and compile the executable yourself.

---

## Usage

1. Open the Yeeshkul-helper application. It will now load the Yeeshkul front page.
2. Log in if you are not already logged in (the application uses an embedded Internet Explorer, so if you are already logged in in Internet Explorer, you will be logged in here as well).
3. Navigate to the list you want to download (for example Lists => Pink Floyd Audio List).
4. Click "Download all referenced torrents".
5. Pick a folder where you want to store the torrent files.
6. Do something else while the application works. This will take some time, as the application has to "click" all links and download all torrent files in the thread.
7. When the application is finished, it gives you a summary, and saves a list of completed and failed downloads as "log.txt".
8. Use your favorite torrent application, and add the downloaded files.

Enjoy!

---

## How does it work?

The tool has an embedded web browser, which allows you to browse Yeeshkul just like you do in your regular web browser. When you're logged in, and are reading a thread that contains links to torrent posts, you can click "Download all referenced torrents", and the application will download all torrents that are linked to from the current page. It does this by scanning the HTML source code, looking for links to "showthread.php", and it will then visit every one of these links, looking for torrent attachments, which it will download.


PS: Yes, I am aware that this could be solved in a much better way than emulating a web browser that "clicks" all links, but this was the easiest way I could think of, and I didn't want to spend more time than necessary making a tool I'll probably use once.