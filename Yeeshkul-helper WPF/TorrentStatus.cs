﻿namespace Yeeshkul_helper_WPF
{
    public enum TorrentStatus
    {
        Waiting,
        Downloading,
        Done,
        Failed,
        TimedOut
    }
}