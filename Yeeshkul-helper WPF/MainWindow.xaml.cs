﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Yeeshkul_helper_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _isIdle = true;
        public bool IsIdle
        {
            get => _isIdle;
            set {
                _isIdle = value;
                OnPropertyChanged();
            }
        }

        public int MaxCount => ToDownload?.Count ?? 1;
        public int DoneCount => ToDownload?.Count(p => p.Status != TorrentStatus.Waiting) ?? 0;

        private ObservableCollection<Torrent> _toDownload = new ObservableCollection<Torrent>();
        public ObservableCollection<Torrent> ToDownload
        {
            get => _toDownload;
            set
            {
                _toDownload = value;
                OnPropertyChanged();
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            wbBrowser.Navigate("http://yeeshkul.com/");
        }

        private async void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            IsIdle = false;
            var fbDialog = new FolderBrowserDialog();
            if (fbDialog.ShowDialog() != System.Windows.Forms.DialogResult.OK) return;

            var s = Regex.Split(wbBrowser.DocumentText, "showthread.php\\?");
            ToDownload.Clear();
            foreach (var url in s.Skip(1).Select(p => new Torrent(p)))
                if (url.Url != "http://yeeshkul.com/forum/showthread.php?42675-Pink-Floyd-Audio-List")
                    ToDownload.Add(url);

            OnPropertyChanged(nameof(MaxCount));
            OnPropertyChanged(nameof(DoneCount));

            foreach (var item in ToDownload)
            {
                try
                {
                    await item.Download(wbBrowser, fbDialog.SelectedPath);
                }catch(Exception ex)
                {
                    item.Status = TorrentStatus.Failed;
                }
                lbDownloads.ScrollIntoView(item);
                OnPropertyChanged(nameof(DoneCount));
            }
            IsIdle = true;

            System.IO.File.WriteAllLines(System.IO.Path.Combine(fbDialog.SelectedPath, "log.txt"), ToDownload.Select(p => p.Status + "; " + p.Url));
            System.Windows.MessageBox.Show($"Downloaded {ToDownload.Count(p => p.Status == TorrentStatus.Done):n0}, failed {ToDownload.Count(p => p.Status == TorrentStatus.Failed):n0}, timed out {ToDownload.Count(p => p.Status == TorrentStatus.TimedOut):n0}. See the list to the right (double click an item to open it in your web browser), or log.txt for details.", "Finished");
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void lbDownloads_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if ((e.Source as System.Windows.Controls.ListBox)?.SelectedItem is Torrent t)
                try
                {
                    System.Diagnostics.Process.Start(t.Url);
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show("Couldn't open forum post: " + ex.Message, "Failed", MessageBoxButton.OK, MessageBoxImage.Error);
                }
        }
    }
}
