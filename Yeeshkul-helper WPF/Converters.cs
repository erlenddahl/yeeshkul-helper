﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Yeeshkul_helper_WPF
{
    [ValueConversion(typeof(TorrentStatus), typeof(Brush))]
    public class StatusToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var v = (TorrentStatus)value;
            switch (v)
            {
                case TorrentStatus.Waiting:
                    return Brushes.DarkGray;
                case TorrentStatus.Done:
                    return Brushes.DarkGreen;
                case TorrentStatus.Failed:
                    return Brushes.DarkRed;
                case TorrentStatus.TimedOut:
                    return Brushes.DarkRed;
                case TorrentStatus.Downloading:
                default:
                    return Brushes.Black;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}