﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Yeeshkul_helper_WPF
{
    public class Torrent: INotifyPropertyChanged
    {
        private string _url;
        private TorrentStatus _status = TorrentStatus.Waiting;

        public TorrentStatus Status
        {
            get => _status;
            set
            {
                if (_status == value) return;
                _status = value;
                OnPropertyChanged();
            }
        }

        public string Url
        {
            get => _url;
            set
            {
                if (_url == value) return;
                _url = value;
                OnPropertyChanged();
            }
        }

        public string Title => Url.Split('?')[1];

        public Torrent(string url)
        {
            Url = "http://yeeshkul.com/forum/showthread.php?" + url.Split('"', '?', '&', '#')[0].Trim();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        internal string html = null;
        internal async Task Download(DownloadWebBrowser wbBrowser, string path)
        {
            wbBrowser.DocumentCompleted += WbBrowser_DocumentCompleted;
            Status = TorrentStatus.Downloading;
            html = null;
            wbBrowser.Navigate(Url);

            if(!await WaitForHtml(30))
                return;

            if (!html.Contains("attachment.php?"))
            {
                Status = TorrentStatus.Failed;
                return;
            }

            var href = Regex.Split(html, "href=\"attachment.php\\?");
            foreach(var link in href.Skip(1))
            {
                if (!link.Contains("</a>")) continue;
                var url = "http://yeeshkul.com/forum/attachment.php?" + link.Split('"', '\'')[0].Trim();
                await DownloadTorrent(wbBrowser, url, path, link.KeepBetween(">", "</a>").MakeSafeForFilename());
            }
             
            wbBrowser.DocumentCompleted -= WbBrowser_DocumentCompleted;
            Status = TorrentStatus.Done;
        }

        private void WbBrowser_DocumentCompleted(object sender, System.Windows.Forms.WebBrowserDocumentCompletedEventArgs e)
        {
            html = (sender as DownloadWebBrowser).DocumentText;
        }

        private async Task DownloadTorrent(DownloadWebBrowser wbBrowser, string url, string path, string filename)
        {
            await Task.Run(() => wbBrowser.DownloadFile(url, System.IO.Path.Combine(path, "Yeeshkul-torrent - " + filename)));
        }

        private async Task<bool> WaitForHtml(int timeout)
        {
            return await Task.Run(() =>
            {
                var start = DateTime.Now;
                while (string.IsNullOrEmpty(html))
                {
                    Thread.Sleep(200);
                    if (DateTime.Now.Subtract(start).TotalSeconds > timeout)
                    {
                        Status = TorrentStatus.TimedOut;
                        return false;
                    }
                }

                return true;
            });
        }
    }
}